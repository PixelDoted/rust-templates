use std::collections::HashMap;

use actix_web::{HttpRequest, HttpResponse, Error, HttpResponseBuilder, body::MessageBody};

pub async fn to_response<C>(content: C, content_type: &str) -> Result<HttpResponse, Error> where C: MessageBody + 'static {
    let response = HttpResponse::Ok().content_type(content_type).body(content);
    Ok(response)
}

pub async fn read_bytes(req: HttpRequest, mut res: HttpResponseBuilder, path: &str, content_type: &str) -> Result<HttpResponse, Error> {
    match std::fs::read(path) {
        Ok(content) => {
            Ok(res.content_type(content_type)
                .body(content))
        },
        Err(e) => {
            println!("read_bytes({}): error, {:?}", req.match_info().query("id").parse::<String>().unwrap(), e);
            Ok(HttpResponse::NoContent().finish())
        }
    }
}

pub async fn read_string(req: HttpRequest, path: &str, content_type: &str) -> Result<HttpResponse, Error> {
    match std::fs::read_to_string(path) {
        Ok(content) => {
            Ok(HttpResponse::Ok()
                .content_type(content_type)
                .body(content))
        },
        Err(e) => {
            println!("read_string({}): error, {:?}", req.match_info().query("id").parse::<String>().unwrap(), e);
            Err(actix_web::Error::from(e))
        }
    }
}

pub async fn query_from_string(query: &str) -> HashMap<String, String> {
    actix_web::web::Query::from_query(query).unwrap().0
}
