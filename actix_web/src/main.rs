use actix_web::{get, web, App, HttpServer, HttpResponse, HttpRequest, Error};
use askama::Template;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
mod utils;

#[derive(Template)]
#[template(path = "index.html")]
struct Index;

#[get("/")]
async fn index() -> Result<HttpResponse, Error> {
    let page = Index.render().unwrap();
    utils::to_response(page, "text/html").await
}

#[derive(Template)]
#[template(path = "hello.html")]
struct HelloPage {
    name: String,
}

#[get("/hello/{name}")]
async fn hello(name: web::Path<String>) -> Result<HttpResponse, Error> {
    let page = HelloPage {
        name: name.clone(),
    }.render().unwrap();
    utils::to_response(page, "text/html").await
}

#[get("/image")]
async fn image(req: HttpRequest) -> Result<HttpResponse, Error> {
    utils::read_bytes(req, HttpResponse::Ok(), "./hello.png", "image/png").await
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file("./ssl/key.pem", SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file("./ssl/cert.pem").unwrap();

    HttpServer::new(|| {
        App::new().service(index).service(hello).service(image)
    })
    .bind_openssl("127.0.0.1:8443", builder)?
    .run()
    .await
}