use eframe::{egui, App};

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[cfg_attr(feature = "persistence", derive(serde::Deserialize, serde::Serialize))]
#[cfg_attr(feature = "persistence", serde(default))] // if we add new fields, give them default values when deserializing old state
pub struct Example {
    pub value: f32,
    pub checked: bool,
    pub label: String,
}

impl Example {
    pub fn new() -> Self {
        Self {
            value: 0.0,
            checked: false,
            label: String::new(),
        }
    }
}

// EGui Window Data
impl App for Example {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        ctx.set_visuals(egui::style::Visuals::dark());

        // Top Panel Example
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                // add a menu bar
                ui.menu_button("App", |ui| {
                    // add a menu button
                    if ui.button("Quit").clicked() {
                        // add a quit button
                        frame.quit(); // if clicked, quit the current eframe
                    }
                });
            });
        });

        // Side Panel Example
        egui::SidePanel::left("side_panel").show(ctx, |ui| {
            ui.heading("Side Panel"); // add a heading saying "Side Panel"

            ui.horizontal(|ui| {
                // addd a horizontal layout
                ui.label("Write something: "); // add a label saying "Write something: "
                ui.text_edit_singleline(&mut self.label); // add a text field
            });

            ui.add(egui::Slider::new(&mut self.value, 0.0..=10.0).text("value")); // add a slider to the ui
            if ui.button("Increment").clicked() {
                // add a button and check if it was clicked
                self.value += 1.0; // if clicked, increment value by 1.0
            }

            ui.with_layout(egui::Layout::bottom_up(egui::Align::LEFT), |ui| {
                // add a layout to the ui, to align content to the bottom left
                ui.horizontal(|ui| {
                    // add a horizontal layout
                    ui.spacing_mut().item_spacing.x = 0.0; // add some spacing
                    ui.label("Bottom Left"); // add a label saying "Bottom Left"
                });
            });
        });

        // Center Panel Example
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Central Panel"); // add a heading saying "Central Panel"
            ui.label("Hello"); // add a label saying "Hello"

            egui::warn_if_debug_build(ui); // show a colored label if we are in debug mode
        });

        // Window Example
        egui::Window::new("Window").show(ctx, |ui| {
            // create a new window
            ui.checkbox(&mut self.checked, "hi"); // add a checkbox
            if self.checked {
                ui.label("Hello");
            }
            // say "Hello" if it is checked
            else {
                ui.label("Window");
            } // say "Window" if it isn't checked
        });
    }
}
