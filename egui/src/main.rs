mod app;

#[tokio::main]
async fn main() {
    let app = app::Example::new();
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "Rust Template",
        native_options,
        Box::new(|_cc| Box::new(app)),
    );
}
