const codec = 'video/webm; codecs="vp9, opus"';

let media_interval;
let media_video = document.getElementById("video");

let media_offset = 0;
let media_bitrate = 1520000; // TODO: read bitrate from server
let media_id = "video";

let media_tick = 0;
let media_queue = [];
let media_finished = false;

let media_source;
let media_buffer;

if ('MediaSource' in window) {
    media_source = new MediaSource;
    media_video.src = (window.URL || window.webkitURL).createObjectURL(media_source);

    media_source.addEventListener('sourceopen', function(e) {
        media_buffer = media_source.addSourceBuffer(codec);
        media_buffer.addEventListener('updateend', _ => {
            if (media_queue.length > 0) {
                media_buffer.appendBuffer(media_queue.pop());
            }
        });

        media_video.addEventListener('play', _ => stop_interval());
        media_video.addEventListener('pause', _ => start_media_interval());
        media_video.addEventListener('timeupdate', _ => {
            media_tick++;
            if (media_tick >= 5) {
                read();
                media_tick = 0;
            }
        });

        start_media_interval();
        read();
        media_video.play();
    });
}

function start_media_interval() {
    if (media_interval === undefined) {
        media_interval = setInterval(function(e) {
            read();
        }, 10000);
    }
}

function stop_interval() {
    if (media_interval !== undefined) {
        clearInterval(media_interval);
        media_interval = undefined;
    }
}

function read() {
    if (media_finished) {
        return;
    }

    let xhr = new XMLHttpRequest();
    xhr.open("get", "/videoplayback?id=" + media_id + "&offset=" + media_offset + "&bitrate=" + media_bitrate);
    xhr.responseType = "arraybuffer";

    xhr.onload = function(e) {
        if (xhr.response.byteLength == 0) {
            media_finished = true;
            stop_interval();
            media_source.endOfStream();
            return;
        }

        media_offset += xhr.response.byteLength;
        if (media_buffer.updating || media_queue.length > 0) {
            media_queue.push(xhr.response);
        } else {
            media_buffer.appendBuffer(xhr.response);
        }
    };
    xhr.send();
}