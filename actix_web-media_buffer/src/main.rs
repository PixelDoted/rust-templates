use actix_web::{get, App, HttpServer, HttpResponse, HttpRequest, Error};
use askama::Template;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

mod utils;
mod media;

#[derive(Template)]
#[template(path = "index.html")]
struct Index;

#[get("/")]
async fn index() -> Result<HttpResponse, Error> {
    let page = Index.render().unwrap();
    utils::to_response(page, "text/html").await
}

#[get("/scripts/mediaplayer.js")]
async fn mediaplayer(req: HttpRequest) -> Result<HttpResponse, Error> {
    utils::read_bytes(req, HttpResponse::Ok(), "./static/mediaplayer.js", "application/javascript").await
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file("./ssl/key.pem", SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file("./ssl/cert.pem").unwrap();

    HttpServer::new(|| {
        App::new().wrap(actix_web::middleware::Compress::default()).service(index).service(mediaplayer).service(media::videoplayback)
    })
    .bind_openssl("127.0.0.1:8443", builder)?
    .run()
    .await
}