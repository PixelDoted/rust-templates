use std::os::unix::prelude::FileExt;
use actix_web::{get, Error, HttpResponse, HttpRequest};
use crate::utils;

#[get("/videoplayback")]
async fn videoplayback(req: HttpRequest) -> Result<HttpResponse, Error> {
    let wrapped_playback = get_playback_info(req.query_string()).await;
    if wrapped_playback.is_none() {
        return Ok(HttpResponse::BadRequest().finish());
    }

    let playback = wrapped_playback.unwrap();
    let file = std::fs::File::open(format!("./{}.webm", playback.0)).unwrap();
    let length = (file.metadata().unwrap().len() - playback.1) as usize;
    if length == 0 {
        return Ok(HttpResponse::NoContent().finish());
    }
    
    let mut buf: Vec<u8> = vec![0; playback.2.min(length)];
    let _ = file.read_at(&mut buf, playback.1);

    utils::to_response(buf, "application/octet-stream").await
}

async fn get_playback_info(query_string: &str) -> Option<(String, u64, usize)> {
    let query = utils::query_from_string(query_string).await;
    let wrapped_id = query.get("id");
    let wrapped_offset = query.get("offset");
    let wrapped_bitrate = query.get("bitrate");
    if wrapped_offset.is_none() || wrapped_bitrate.is_none() || wrapped_id.is_none() {
        return None;
    }

    let offset = wrapped_offset.unwrap().parse::<u64>();
    let bitrate = wrapped_bitrate.unwrap().parse::<u64>();
    if offset.is_err() || bitrate.is_err() {
        return None;
    }

    Some((wrapped_id.unwrap().clone(), offset.unwrap(), bitrate.unwrap() as usize))
}